import { Component } from '@angular/core';
import classnames from 'classnames';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'app';
    show = true;
    class = 'hover';

    changingVisibility() {
        window.console.warn('HIZO CLICK');
        window.console.warn(this.show);
        this.show = !this.show;
        window.console.warn(this.show);
    }
}
